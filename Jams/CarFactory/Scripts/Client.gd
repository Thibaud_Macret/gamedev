extends Node2D

@export var client_id:int
var commande:Array

func genere_commande():
	commande = [[0,randi()%3], [1,randi()%3], [2,randi()%3]]
	$Demande/Carre1.frame = 0
	$Demande/Carre2.frame = 0
	$Demande/Carre3.frame = 0
	
	var char_tex = load(get_sprite_res(commande[0]))
	$Demande/Demande1.set_texture(char_tex)
	var char_tex2 = load(get_sprite_res(commande[1]))
	$Demande/Demande2.set_texture(char_tex2)
	var char_tex3 = load(get_sprite_res(commande[2]))
	$Demande/Demande3.set_texture(char_tex3)
	
func get_sprite_res(objet) -> String:
	var str:String = "res://Sprites/"
	
	if(objet[0] == 0):
		str += "carrom"
	elif(objet[0] == 1):
		str += "moteur"
	elif(objet[0] == 2):
		str += "roue"
	
	if(objet[1] == 0):
		str += "A"
	elif(objet[1] == 1):
		str += "B"
	elif(objet[1] == 2):
		str += "C"
	
	str += ".png"
	
	return str

func livraison(objet:Array):
	if(objet in commande) :
		var rang:int = commande.find(objet)
		$Demande.get_child(rang*2).frame = 1
		commande[rang] = null
		if(commande[0] == commande[1] and commande[0] == commande[2]):
			genere_commande()
			get_parent().score()

func _physics_process(delta):
	if(commande.size() == 0) :
		genere_commande()
	if(Input.is_action_just_pressed("ui_cancel")):
		genere_commande()
