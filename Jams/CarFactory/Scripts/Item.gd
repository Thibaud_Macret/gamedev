extends RigidBody2D

var objet:int
var variante:int

func _ready():
	objet = randi()%3
	variante = randi()%3
	
	var char_tex = load(get_sprite_res())
	$Sprite.set_texture(char_tex)
	
	if(objet == 0):
		$CollisionShapeCaro.disabled = false
	elif(objet == 1):
		$CollisionShapeMoteur.disabled = false
	elif(objet == 2):
		$CollisionShapeRoue.disabled = false

func get_sprite_res() -> String:
	var str:String = "res://Sprites/"
	
	if(objet == 0):
		str += "carro"
	elif(objet == 1):
		str += "moteur"
	elif(objet == 2):
		str += "roue"
	
	if(variante == 0):
		str += "A"
	elif(variante == 1):
		str += "B"
	elif(variante == 2):
		str += "C"
	
	str += ".png"
	
	return str

func get_item():
	return [objet,variante]

func _on_body_entered(body):
	get_parent().play_metal()
