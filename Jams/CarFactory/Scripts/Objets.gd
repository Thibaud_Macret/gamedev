extends Node2D

var timePlay:float = 0

func play_metal():
	if (timePlay == 0):
		get_child(randi()%10).play()
		timePlay = randf()*0.6

func _physics_process(delta):
	if(timePlay > 0):
		timePlay -= delta
		if(timePlay < 0):
			timePlay = 0
