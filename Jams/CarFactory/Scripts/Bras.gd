extends StaticBody2D

const speed:int = 4

var is_moving_left:bool = false
var is_moving_right:bool = false
var is_moving_up:bool = false
var is_moving_down:bool = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(is_moving_left):
		if(Input.is_action_just_released("ui_left")):
			is_moving_left = false
	else :
		if(Input.is_action_just_pressed("ui_left")):
			is_moving_left = true

	if(is_moving_right):
		if(Input.is_action_just_released("ui_right")):
			is_moving_right = false
	else :
		if(Input.is_action_just_pressed("ui_right")):
			is_moving_right = true

	if(is_moving_up):
		if(Input.is_action_just_released("ui_up")):
			is_moving_up = false
	else :
		if(Input.is_action_just_pressed("ui_up")):
			is_moving_up = true

	if(is_moving_down):
		if(Input.is_action_just_released("ui_down")):
			is_moving_down = false
	else :
		if(Input.is_action_just_pressed("ui_down")):
			is_moving_down = true
	
	move()

func move():
	if(is_moving_right):
		if(self.position.x < 525):
			self.move_and_collide(Vector2(speed, 0))
			$Aimant.move_aimant(Vector2(speed, 0))
	if(is_moving_left):
		if(self.position.x > -525):
			self.move_and_collide(Vector2(-speed, 0))
			$Aimant.move_aimant(Vector2(-speed, 0))
	if(is_moving_up):
		if(self.position.y > -695):
			self.move_and_collide(Vector2(0, -speed))
			$Aimant.move_aimant(Vector2(0, -speed))
	if(is_moving_down):
		if(self.position.y < -210):
			self.move_and_collide(Vector2(0, speed))
			$Aimant.move_aimant(Vector2(0, speed))


func _on_zone_livraison_body_entered(body):
	pass # Replace with function body.
