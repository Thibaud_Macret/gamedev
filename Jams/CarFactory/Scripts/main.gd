extends Node2D

var timer: float = 0
var nbCommandes:int = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timer += delta
	$Header/ProgressBar.value = timer
	if(timer > 110):
		Score.score = str(nbCommandes)
		get_tree().change_scene_to_file("res://Scenes/fin.tscn")

func score():
	nbCommandes += 1
	$Beep.play()
