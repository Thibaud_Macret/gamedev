extends Area2D

var status:bool = false
var hooked_items:Array = []
var spriteTimer:float = 0

func _physics_process(delta):
	if(Input.is_action_just_pressed("ui_accept")):
		status = !status
		$Sprite.visible = status
		if(!status):
			for item in (get_overlapping_bodies()):
				item.gravity_scale = 1
	
	spriteTimer += delta
	if(spriteTimer > 0.2):
		spriteTimer = 0
		if $Sprite.frame == 2 :
			$Sprite.frame = 0
		else :
			$Sprite.frame += 1

func move_aimant(vec:Vector2):
	self.position.x = $"../Tete".position.x
	self.position.y = $"../Tete".position.y
	if(status):
		for item in (get_overlapping_bodies()):
			item.move_and_collide(vec)
			item.gravity_scale = 0

func _on_body_exited(body):
	body.gravity_scale = 1
